<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '8a6e09648df07cc421b7abcf40fd5222583856b9',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '8a6e09648df07cc421b7abcf40fd5222583856b9',
            'dev_requirement' => false,
        ),
        'razorpay/razorpay' => array(
            'pretty_version' => '2.8.1',
            'version' => '2.8.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../razorpay/razorpay',
            'aliases' => array(),
            'reference' => '4ad7b6a5bd9896305858ec0a861f66020e39f628',
            'dev_requirement' => false,
        ),
        'rmccue/requests' => array(
            'pretty_version' => 'v1.8.0',
            'version' => '1.8.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../rmccue/requests',
            'aliases' => array(),
            'reference' => 'afbe4790e4def03581c4a0963a1e8aa01f6030f1',
            'dev_requirement' => false,
        ),
        'stripe/stripe-php' => array(
            'pretty_version' => 'v7.108.0',
            'version' => '7.108.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../stripe/stripe-php',
            'aliases' => array(),
            'reference' => '81524a3087612f1b41846b9cd6deb80150af985b',
            'dev_requirement' => false,
        ),
    ),
);
