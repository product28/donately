<?php
include VIEWPATH . 'admin/header.php';
?>
<div class="app-content">
    <div class="container-fluid">

        <!--  Page-header opened -->
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard'); ?>"><i class="fe fe-settings mr-1"></i> Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo dt_translate('setting'); ?></li>
            </ol>
        </div>
        <!--  Page-header closed -->

        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">

                            <div class="col-lg-3 col-md-12">
                                <div class="thumbnail mb-xl-0">
                                    <div class="caption">
                                        <h4><strong><?php echo dt_translate('site_setting');?></strong></h4>
                                        <p class="mt-2">
                                            <a href="<?php echo base_url('admin/setting'); ?>" class="btn-link" role="button"><?php echo dt_translate('manage'); ?></a>
                                            <a href="<?php echo base_url('admin/setting'); ?>" class="btn btn-secondary btn-sm float-right" role="button"><i class="fe fe-settings"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-12">
                                <div class="thumbnail mb-xl-0">
                                    <div class="caption">
                                        <h4><strong><?php echo dt_translate('currency'); ?></strong></h4>
                                        <p class="mt-2">
                                            <a href="<?php echo base_url('admin/currency'); ?>" class="btn-link" role="button"><?php echo dt_translate('manage'); ?></a>
                                            <a href="<?php echo base_url('admin/add-currency'); ?>" class="btn btn-secondary btn-sm float-right" role="button"><i class="fe fe-plus"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-12">
                                <div class="thumbnail mb-xl-0">
                                    <div class="caption">
                                        <h4><strong><?php echo dt_translate('language'); ?></strong></h4>
                                        <p class="mt-2">
                                            <a href="<?php echo base_url('admin/language'); ?>" class="btn-link" role="button"><?php echo dt_translate('manage'); ?></a>
                                            <a href="<?php echo base_url('admin/add-language'); ?>" class="btn btn-secondary btn-sm float-right" role="button"><i class="fe fe-plus"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-12">
                                <div class="thumbnail mb-xl-0">
                                    <div class="caption">
                                        <h4><strong><?php echo dt_translate('department'); ?></strong></h4>
                                        <p class="mt-2">
                                            <a href="<?php echo base_url('admin/donation-category'); ?>" class="btn-link" role="button"><?php echo dt_translate('manage'); ?></a>
                                            <a href="<?php echo base_url('admin/add-donation-category'); ?>" class="btn btn-secondary btn-sm float-right" role="button"><i class="fe fe-plus"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">

                            <div class="col-lg-3 col-md-12">
                                <div class="thumbnail mb-xl-0">
                                    <div class="caption">
                                        <h4><strong><?php echo dt_translate('items'); ?></strong></h4>
                                        <p class="mt-2">
                                            <a href="<?php echo base_url('admin/items'); ?>" class="btn-link" role="button"><?php echo dt_translate('manage'); ?></a>
                                            <a href="<?php echo base_url('admin/add-item'); ?>" class="btn btn-secondary btn-sm float-right" role="button"><i class="fe fe-plus"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-12">
                                <div class="thumbnail mb-xl-0">
                                    <div class="caption">
                                        <h4><strong><?php echo dt_translate('expense_category'); ?></strong></h4>
                                        <p class="mt-2">
                                            <a href="<?php echo base_url('admin/expense-category'); ?>" class="btn-link" role="button"><?php echo dt_translate('manage'); ?></a>
                                            <a href="<?php echo base_url('admin/add-expense-category'); ?>" class="btn btn-secondary btn-sm float-right" role="button"><i class="fe fe-plus"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-12">
                                <div class="thumbnail mb-xl-0">
                                    <div class="caption">
                                        <h4><strong><?php echo dt_translate('news'); ?></strong></h4>
                                        <p class="mt-2">
                                            <a href="<?php echo base_url('admin/manage-news'); ?>" class="btn-link" role="button"><?php echo dt_translate('manage'); ?></a>
                                            <a href="<?php echo base_url('admin/add-news'); ?>" class="btn btn-secondary btn-sm float-right" role="button"><i class="fe fe-plus"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-12">
                                <div class="thumbnail mb-xl-0">
                                    <div class="caption">
                                        <h4><strong><?php echo dt_translate('events'); ?></strong></h4>
                                        <p class="mt-2">
                                            <a href="<?php echo base_url('admin/manage-events'); ?>" class="btn-link" role="button"><?php echo dt_translate('manage'); ?></a>
                                            <a href="<?php echo base_url('admin/add-event'); ?>" class="btn btn-secondary btn-sm float-right" role="button"><i class="fe fe-plus"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-lg-3 col-md-12">
                                <div class="thumbnail mb-xl-0">
                                    <div class="caption">
                                        <h4><strong><?php echo dt_translate('projects'); ?></strong></h4>
                                        <p class="mt-2">
                                            <a href="<?php echo base_url('admin/manage-projects'); ?>" class="btn-link" role="button"><?php echo dt_translate('manage'); ?></a>
                                            <a href="<?php echo base_url('admin/add-project'); ?>" class="btn btn-secondary btn-sm float-right" role="button"><i class="fe fe-plus"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- col end -->
        </div>

    </div>
</div>
<?php
include VIEWPATH . 'admin/footer.php';
?>