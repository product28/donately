<?php
$setting_url_segment = trim($this->uri->segment(2));


$setting_url_class='tab_inactive';
$email_setting_url_class='tab_inactive';
$payment_setting_url_class='tab_inactive';
$module_setting_url_class='tab_inactive';
$seo_setting_url_class='tab_inactive';
$comment_setting_url_class='tab_inactive';

if(isset($setting_url_segment) && $setting_url_segment=='email-setting'){
    $email_setting_url_class='tab_active';
}elseif (isset($setting_url_segment) && $setting_url_segment=='payment-setting'){
    $payment_setting_url_class='tab_active';
}elseif (isset($setting_url_segment) && $setting_url_segment=='module-setting'){
    $module_setting_url_class='tab_active';
}elseif (isset($setting_url_segment) && $setting_url_segment=='seo'){
    $seo_setting_url_class='tab_active';
}elseif (isset($setting_url_segment) && $setting_url_segment=='comment'){
    $comment_setting_url_class='tab_active';
}else{
    $setting_url_class='tab_active';
}

?>
<ul class="nav text-left" id="website_topbar">
    <li class="nav-item">
        <a role="tab" class="nav-link <?php echo $setting_url_class; ?> show" href="<?php echo base_url('admin/setting'); ?>" aria-selected="true">
            <span><?php echo dt_translate('site_setting'); ?></span>
        </a>
    </li>
    <li class="nav-item">
        <a role="tab" class="nav-link show <?php echo $email_setting_url_class; ?>" href="<?php echo base_url('admin/email-setting'); ?>" aria-selected="false">
            <span><?php echo dt_translate('email_setting'); ?></span>
        </a>
    </li>
    <li class="nav-item">
        <a role="tab" class="nav-link show <?php echo $payment_setting_url_class; ?>" href="<?php echo base_url('admin/payment-setting'); ?>" aria-selected="false">
            <span><?php echo dt_translate('payment_setting'); ?></span>
        </a>
    </li>
    <li class="nav-item">
        <a role="tab" class="nav-link show <?php echo $module_setting_url_class; ?>" href="<?php echo base_url('admin/module-setting'); ?>" aria-selected="false">
            <span><?php echo dt_translate('module'); ?></span>
        </a>
    </li>
    <li class="nav-item">
        <a role="tab" class="nav-link show <?php echo $seo_setting_url_class; ?>" href="<?php echo base_url('admin/seo'); ?>" aria-selected="false">
            <span><?php echo dt_translate('seo'); ?></span>
        </a>
    </li>
    <li class="nav-item">
        <a role="tab" class="nav-link show <?php echo $comment_setting_url_class; ?>" href="<?php echo base_url('admin/comment'); ?>" aria-selected="false">
            <span>CommentBox</span>
        </a>
    </li>
</ul>