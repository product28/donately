<?php
include VIEWPATH . 'admin/header.php';
?>
<div class="app-content">
    <div class="container-fluid">
    <input type="hidden" value="<?php echo $donation_chart_month_name; ?>" id="donation_chart_month_name"/>
    <input type="hidden" value="<?php echo $donation_chart_amount; ?>" id="donation_chart_amount"/>
    <input type="hidden" value="<?php echo dt_translate('year').' '.date('Y'); ?>" id="donation_year_lang"/>
        <!-- Banner opened -->

        <div class="row" style="margin-top: 140px;">
            <div class="col-xl-12">
            </div>
        </div>
        <!-- Banner opened -->
        <div class="row">
            <div class="col-md-12">
                <?php if($this->login_type=='A' && ($total_donation_category['total_donation_category']==0 || count($payment)==0)): ?>
                    <div class="card">
                        <div class="card-header">
                            Mandatory Update
                        </div>
                        <div class="card-body">

                            <?php if(isset($total_donation_category['total_donation_category']) && $total_donation_category['total_donation_category']==0): ?>
                                <div class="alert alert-info">
                                    Please add at least one donation category to start accepting donation. <a  href="<?php echo base_url('admin/donation-category'); ?>">Click Here</a>
                                </div>

                            <?php endif; ?>

                            <?php if(isset($payment) && count($payment)==0): ?>
                                <div class="alert alert-info">
                                    Please add at least one payment method to start accepting donation. <a href="<?php echo base_url('admin/payment-setting'); ?>">Click Here</a>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <!-- row opened -->
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="">
                                <p class="mb-2 h6"><?php echo dt_translate('donator'); ?></p>
                                <h2 class="mb-1 "><?php echo $total_donator; ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="">
                                <p class="mb-2 h6"><?php echo dt_translate('total')." ".dt_translate('donation'); ?></p>
                                <h2 class="mb-1 "><?php echo dt_price_format($total_donation); ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="">
                                <p class="mb-2 h6"><?php echo dt_translate('total')." ".dt_translate('expense'); ?></p>
                                <h2 class="mb-1 "><?php echo dt_price_format($total_expense); ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card overflow-hidden">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="">
                                <p class="mb-2 h6"><?php echo dt_translate('contact_us')." ".dt_translate('request'); ?></p>
                                <h2 class="mb-1 "><?php echo $total_contact_request; ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row closed -->
    </div>
</div>
<?php
include VIEWPATH . 'admin/footer.php';
?>
