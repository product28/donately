<div class="modal fade" id="delete-record">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php
            $attributes = array('id' => 'DeleteRecordForm', 'name' => 'DeleteRecordForm', 'method' => "post");
            echo form_open('', $attributes);
            ?>
            <input type="hidden" id="record_id"/>
            <input type="hidden" id="form_action"/>
            <div class="modal-header">
                <h4 id='some_name' class="modal-title font_size_18"><?php echo dt_translate('delete'); ?></h4>
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <h5 class="font-size-lg"><?php echo dt_translate('delete_confirm'); ?></h5>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary font_size_12" onclick="DeleteRecord()" href="javascript:void(0)" id="RecordDelete" ><?php echo dt_translate('confirm'); ?></a>
                <button data-dismiss="modal" class="btn btn-danger font_size_12" type="button"><?php echo dt_translate('close'); ?></button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Footer opened -->
<!--<footer class="footer-main">
    <div class="container">
        <div class="  mt-2 mb-2 text-center">
            &COPY; <?php echo date('Y') . " " . dt_get_CompanyName(); ?>. <?php echo dt_translate('rights_reserved_message') ?>
        </div>
    </div>
</footer>-->
<!-- Footer closed -->
</div>
<!-- Dashboard js -->
<script type="text/javascript" src="<?php echo base_url('assets/global/js/jquery-3.4.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/global/js/bootstrap.bundle.min.js');?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/admin/plugins/chart/chart.min.js');?>"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/index.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/plugins/horizontal-menu/horizontal.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/custom.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/global/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/global/js/additional-methods.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/functions.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/donation_custom.js');?>"></script>
</body>
</html>
