<?php
include VIEWPATH . 'admin/header.php';
?>
<div class="container-fluid  content-area">
    <div class="section">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-home mr-1"></i> <?php echo dt_translate('dashboard'); ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo base_url('admin/donation'); ?>"><?php echo dt_translate('manage') . " " . dt_translate('donation') ?></a></li>
            </ol>
            <div class="ml-auto">
                <a href="<?php echo base_url('admin/donation-export'); ?>" class="btn btn-warning btn-icon btn-sm text-white mr-2"> <span> <i class="fa fa-file-excel-o"></i></span> <?php echo dt_translate('export'); ?></a>
                <a href="<?php echo base_url('admin/add-donation-category'); ?>" class="btn btn-secondary btn-icon btn-sm text-white mr-2"> <span> <i class="fa fa-plus"></i> </span> <?php echo dt_translate('add') . " " . dt_translate('department'); ?> </a>
                <a href="<?php echo base_url('admin/add-donation'); ?>" class="btn btn-primary btn-icon btn-sm text-white mr-2"> <span> <i class="fa fa-plus"></i> </span> <?php echo dt_translate('add') . " " . dt_translate('donation'); ?> </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <?php $this->load->view('message'); ?>
                <div class="card">
                    <div class="card-header">
                        <?php echo $title; ?>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table id="example" class="table table-bordered text-nowrap mb-0">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th><?php echo dt_translate('name') ?></th>
                                    <th><?php echo dt_translate('phone') ?></th>
                                    <th><?php echo dt_translate('city') ?></th>
                                    <th><?php echo dt_translate('category') ?></th>
                                    <th><?php echo dt_translate('payment_by') ?></th>
                                    <th><?php echo dt_translate('amount') ?></th>
                                    <th><?php echo dt_translate('date') ?></th>
                                    <?php if($this->login_type=='A'): ?>
                                        <th class="text-center"><?php echo dt_translate('action')?></th>
                                    <?php endif; ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (isset($donation_data) && count($donation_data) > 0): ?>
                                    <?php
                                    $display_count=1;
                                    foreach ($donation_data as $val):
                                        $payment_type="";
                                        if($val['type']=='CQ'){
                                            $payment_type="<span class='badge badge-warning'>".dt_translate('cheque')."-".$val['cheque_no']."</span>";
                                        }else if($val['type']=='S'){
                                            $payment_type="<span class='badge badge-info'>Stripe</span>";
                                        }else if($val['type']=='P'){
                                            $payment_type="<span class='badge badge-success'>PayPal</span>";
                                        }else if($val['type']=='R'){
                                            $payment_type="<span class='badge badge-warning'>Razorpay</span>";
                                        }else{
                                            $payment_type="<span class='badge badge-warning'>".dt_translate('cash')."</span>";
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $display_count; ?></td>
                                            <td>
                                                <?php echo escape_data($val['first_name'])." ".escape_data($val['last_name']); ?><br/>
                                                <small class="badge badge-default"><?php echo escape_data($val['email']); ?></small>
                                            </td>
                                            <td><?php echo escape_data($val['phone']); ?></td>
                                            <td><?php echo escape_data($val['city']); ?></td>
                                            <td><?php echo escape_data($val['title']); ?></td>
                                            <td><?php echo $payment_type; ?></td>
                                            <td><?php echo dt_price_format($val['amount']); ?></td>
                                            <td><?php echo get_formated_date($val['created_on']); ?></td>
                                            <td class="text-center">
                                                <a target="_blank" href="<?php echo base_url('admin/donation-receipt/' . $val['id']); ?>" class="btn btn-info btn-sm"><i class="fa fa-print"></i></a>
                                                <a href="<?php echo base_url('admin/update-donation/' . $val['id']); ?>" class="btn btn-primary btn-sm"><?php echo dt_translate('update') ?></a>
                                                <a href="javascript:void(0)" data-action="delete-donation" data-toggle="modal" onclick='DeleteConfirm(this)' data-target="#delete-record" data-id="<?php echo (int) $val['id']; ?>"  class="btn btn-danger btn-sm"><?php echo dt_translate('delete') ?></a>
                                            </td>
                                        </tr>
                                        <?php $display_count++; endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php
include VIEWPATH . 'admin/footer.php';
?>
