<?php
include VIEWPATH . 'admin/header.php';
?>
<div class="container-fluid  content-area">
    <div class="section">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-home mr-1"></i> <?php echo dt_translate('dashboard'); ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo base_url('admin/expense'); ?>"><?php echo dt_translate('manage') . " " . dt_translate('expense') ?></a></li>
            </ol>
            <div class="ml-auto">
                <a href="<?php echo base_url('admin/expense-export'); ?>" class="btn btn-warning btn-icon btn-sm text-white mr-2"> <span> <i class="fa fa-file-excel-o"></i></span> <?php echo dt_translate('export'); ?></a>
                <a href="<?php echo base_url('admin/add-expense-category'); ?>" class="btn btn-secondary btn-icon btn-sm text-white mr-2"> <span> <i class="fa fa-plus"></i> </span> <?php echo dt_translate('add') . " " . dt_translate('expense_category'); ?> </a>
                <a href="<?php echo base_url('admin/add-expense'); ?>" class="btn btn-primary btn-icon btn-sm text-white mr-2"> <span> <i class="fa fa-plus"></i> </span> <?php echo dt_translate('add') . " " . dt_translate('expense'); ?> </a>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <?php $this->load->view('message'); ?>
                <div class="card">
                    <div class="card-header">
                        <?php echo dt_translate('expense'); ?>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table id="example" class="table table-bordered text-nowrap mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo dt_translate('expense_category') ?></th>
                                    <th><?php echo dt_translate('amount') ?></th>
                                    <th><?php echo dt_translate('details') ?></th>
                                    <th><?php echo dt_translate('date') ?></th>
                                    <th width="250"  class="text-center"><?php echo dt_translate('action')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (isset($expense_data) && count($expense_data) > 0): ?>
                                    <?php
                                    $display_count=1;
                                    foreach ($expense_data as $val):

                                        $receipt_path="";
                                        if (isset($val['expense_receipt']) && $val['expense_receipt']!= "") {
                                            if (file_exists(FCPATH .uploads_path.'/'. $val['expense_receipt'])) {
                                                $receipt_path = base_url() . uploads_path . '/' . $val['expense_receipt'];
                                            }
                                        }

                                        ?>
                                        <tr>
                                            <td><?php echo $display_count; ?></td>
                                            <td><span class="badge badge-info"><?php echo escape_data($val['category_title']); ?></span></td>
                                            <td><?php echo dt_price_format($val['amount']); ?></td>
                                            <td><?php echo nl2br($val['details']); ?></td>
                                            <td><?php echo get_formated_date($val['expense_date'],"N"); ?></td>
                                            <td class="text-center">
                                                <?php if(isset($receipt_path) && $receipt_path!=""): ?>
                                                        <a class="btn btn-outline-info  btn-sm" href="<?php echo $receipt_path; ?>" download="">Receipt</a>
                                                <?php endif;?>
                                                <a href="<?php echo base_url('admin/update-expense/' . $val['id']); ?>" class="btn btn-primary btn-sm"><?php echo dt_translate('update') ?></a>
                                                <a href="javascript:void(0)" data-action="delete-expense" data-toggle="modal" onclick='DeleteConfirm(this)' data-target="#delete-record" data-id="<?php echo (int) $val['id']; ?>"  class="btn btn-danger btn-sm"><?php echo dt_translate('delete') ?></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $display_count++;
                                    endforeach; ?>
                                <?php else: ?>
                                    <tr class="text-center">
                                        <td colspan="6"><?php echo dt_translate('no_record_found')?></td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<?php
include VIEWPATH . 'admin/footer.php';
?>