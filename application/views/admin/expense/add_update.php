<?php
include VIEWPATH . 'admin/header.php';

$amount = isset($expense_data['amount']) ? $expense_data['amount'] : set_value('amount');
$details = isset($expense_data['details']) ? escape_data($expense_data['details']) : set_value('details');
$category_id = isset($expense_data['category_id']) ? $expense_data['category_id'] : set_value('category_id');
$expense_date = isset($expense_data['expense_date']) ? $expense_data['expense_date'] : set_value('expense_date');
$expense_receipt = isset($expense_data['expense_receipt']) ? $expense_data['expense_receipt'] : set_value('expense_receipt');
$id = isset($expense_data['id']) ? (int) $expense_data['id'] : (int) set_value('id');

$receipt_path="";
if (isset($expense_data['expense_receipt']) && $expense_data['expense_receipt']!= "") {
    if (file_exists(FCPATH .uploads_path.'/'. $expense_data['expense_receipt'])) {
        $receipt_path = base_url() . uploads_path . '/' . $expense_data['expense_receipt'];
    }
}

?>
<div class="container-fluid  content-area">
    <div class="section">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-home mr-1"></i> <?php echo dt_translate('dashboard'); ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo base_url('admin/expense'); ?>"><?php echo dt_translate('manage') . " " . dt_translate('expense') ?></a></li>
            </ol>
        </div>

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <?php $this->load->view('message'); ?>
                <div class="card">
                    <div class="card-header">
                        <?php echo ($id > 0) ? dt_translate('update') : dt_translate('add'); ?> <?php echo dt_translate('expense'); ?>
                    </div>
                    <div class="card-body">
                        <?php
                        $attributes = array('class' => 'col-md-12 mx-auto', 'id' => 'Save_Form', 'name' => 'Save_Form', 'method' => "post");
                        echo form_open_multipart('admin/save-expense', $attributes);
                        ?>

                        <input type="hidden" id="id" name="id" value="<?php echo isset($id) ? $id : 0; ?>"/>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="title"><?php echo dt_translate('expense_category') ?><small class="required">*</small></label>
                                    <div>
                                        <select tabindex="1" required="" id="category_id" name="category_id" class="form-control">
                                            <option value=""><?php echo dt_translate('select')." ".dt_translate('expense_category'); ?></option>
                                            <?php foreach ($app_expense_category as $value): ?>
                                                <option value="<?php echo $value['id']; ?>"  <?php echo ($value['id']==$category_id)?"selected='selected'" :"";?> ><?php echo $value['title']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <?php echo form_error('category_id'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="title"><?php echo dt_translate('amount') ?><small class="required">*</small></label>
                                    <div>
                                        <input type="number" min="1" tabindex="2" required="" autocomplete="off" value="<?php echo isset($amount) ? $amount : ""; ?>" class="form-control" id="amount" name="amount" placeholder="<?php echo dt_translate('amount') ?>">
                                        <?php echo form_error('amount'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="title"><?php echo dt_translate('date') ?><small class="required">*</small></label>
                                    <div>
                                        <input  max="9999-12-30" type="date" tabindex="3" required="" autocomplete="off" value="<?php echo isset($expense_date) ? $expense_date : ""; ?>" class="form-control" id="expense_date" name="expense_date" placeholder="<?php echo dt_translate('date') ?>">
                                        <?php echo form_error('date'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="title"><?php echo dt_translate('details') ?><small class="required">*</small></label>
                                    <div>
                                        <textarea tabindex="4" required="" class="form-control" placeholder="<?php echo dt_translate('details') ?>" id="details" name="details"><?php echo isset($details) ? $details : ""; ?></textarea>
                                        <?php echo form_error('details'); ?>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="old_image" value="<?php echo isset($expense_receipt) ? $expense_receipt : ""; ?>"/>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-label">Expense Receipt</div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="expense_receipt">
                                        <label class="custom-file-label">Choose file</label>
                                    </div>

                                </div>
                            </div>

                            <?php if(isset($receipt_path) && $receipt_path!=""): ?>
                                <div class="col-md-3 mt-5">
                                    <a class="btn btn-outline-info" href="<?php echo $receipt_path; ?>" download="">Receipt</a>
                                </div>
                            <?php endif;?>

                        </div>


                        <br/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <a tabindex="6" href="<?php echo base_url('admin/expense'); ?>" class="btn btn-warning"><?php echo dt_translate('cancel') ?></a>
                                    <button tabindex="5" type="submit" class="btn btn-primary" name="Save" value="Save">
                                        <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                        <?php echo dt_translate('save') ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
include VIEWPATH . 'admin/footer.php';
?>