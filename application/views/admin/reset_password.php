<?php
include VIEWPATH . 'admin/auth_header.php';
?>
    <!-- Page opened -->
    <div class="page">
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="">
            <div class="col col-login mx-auto">
                <div class="text-center">
                    <a href="<?php echo base_url('admin/login');?>"><img src="<?php echo dt_get_CompanyLogo(); ?>" class="header-brand-img desktop-logo h-100 mb-5" alt="<?php echo dt_get_CompanyName(); ?>"></a>
                    <a href="<?php echo base_url('admin/login');?>"><img src="<?php echo dt_get_CompanyLogo(); ?>" class="header-brand-img dark-theme h-100 mb-5 " alt="<?php echo dt_get_CompanyName(); ?>"></a>
                </div>
            </div>
            <!-- container opened -->
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 justify-content-center mx-auto text-center">
                        <div class="card">

                            <div class="row">

                                <div class="col-md-12 col-lg-12 pl-0 ">
                                    <?php
                                    $hidden = array("id" => $id);
                                    $attributes = array('id' => 'change_password_form', 'name' => 'change_password_form', 'method' => "post");
                                    echo form_open('admin/reset-password-action', $attributes, $hidden);
                                    ?>
                                    <div class="card-body p-6 about-con pabout">
                                        <div class="card-title text-center  mb-4"><?php echo dt_translate('forgot_password') ?></div>
                                        <?php $this->load->view('message'); ?>
                                        <div class="form-group">
                                            <input type="password" data-msg-required="<?php echo dt_translate('required_message'); ?>" required="" id="password" minlength="8" autocomplete="off"  name="password" value="" class="form-control" placeholder="<?php echo dt_translate('password'); ?>">
                                            <?php echo form_error('password'); ?>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" data-msg-required="<?php echo dt_translate('required_message'); ?>" required="" id="confirm_password"  autocomplete="off" name="confirm_password" value="" class="form-control" placeholder="<?php echo dt_translate('confirm_password'); ?>">
                                            <?php echo form_error('Cpassword'); ?>
                                        </div>
                                        <div class="form-footer mt-1">
                                            <button class="btn btn-success btn-block" type="submit">
                                                <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                                <?php echo dt_translate('submit'); ?>
                                            </button>
                                        </div>
                                        <hr class="divider">
                                        <div class="text-right text-muted mt-3 ">
                                            <a href="<?php echo base_url("admin/login"); ?>"><?php echo dt_translate('login'); ?></a>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- container closed -->
        </div>
    </div>
    <!-- Page closed -->
<?php
include VIEWPATH . 'admin/auth_footer.php';
?>