<!-- Dashboard js -->
<script type="text/javascript" src="<?php echo base_url('assets/global/js/jquery-3.4.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/global/js/bootstrap.bundle.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/plugins/chart/chart.min.js');?>"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/index.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/plugins/horizontal-menu/horizontal.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/custom.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/global/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/global/js/additional-methods.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/functions.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/donation_custom.js');?>"></script>
</body>
</html>
