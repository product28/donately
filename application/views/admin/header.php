<?php
$login_data = dt_get_CustomerDetails();
$current_page_slug=$this->uri->segment(2);

$front_end_slug=array('manage-news','add-news','update-news','manage-events','add-event','update-event','manage-projects','add-project','update-project','manage-home-content','save-home-content','save-causes','manage-about-us-action','manage-banner','manage-about-us','manage-pages','update-page','manage-gallery','add-gallery','update-gallery','manage-team','add-team','update-team','add-slider','manage-slider','update-slider');

$about_us_active=$cause_active=$news_active=$gallery_active=$slider_active=$team_active=$page_active=$contact_active=$home_content_active="";

$projects_us_active="";
$events_us_active="";
$cms_pages_active="";


$about_array=array("manage-about-us","manage-about-us-action");
$cause_array=array("manage-causes","add-cause","save-causes","delete-causes","update-cause","cause-donation");
$news_array=array("manage-news","add-news","save-news","delete-news","update-news");
$gallery_array=array("manage-gallery","add-gallery","save-gallery","delete-gallery","update-gallery");
$slider_array=array("manage-slider","add-slider","save-slider","delete-slider","update-slider");
$team_array=array("manage-team","add-team","save-team","delete-team","update-team");
$contact_array=array("manage-contact-us");
$banner_array=array("manage-banner");
$projects_array=array('manage-projects','add-project','update-project');
$events_array=array('manage-events','add-event','update-event');
$home_content_array=array("manage-home-content","save-home-content");
$cms_pages_array=array("manage-pages","update-page");


$url_segment = trim($this->uri->segment(2));

if(in_array($url_segment,$about_array)){
    $about_us_active="active";
}elseif (in_array($url_segment,$cause_array)){
    $cause_active="active";
}elseif (in_array($url_segment,$news_array)){
    $news_active="active";
}elseif (in_array($url_segment,$gallery_array)){
    $gallery_active="active";
}elseif (in_array($url_segment,$team_array)){
    $team_active="active";
}elseif (in_array($url_segment,$contact_array)){
    $contact_active="active";
}elseif (in_array($url_segment,$home_content_array)){
    $home_content_active="active";
}elseif (in_array($url_segment,$banner_array)){
    $banner_image_active="active";
}elseif (in_array($url_segment,$banner_array)){
    $banner_image_active="active";
}elseif (in_array($url_segment,$projects_array)){
    $projects_us_active="active";
}elseif (in_array($url_segment,$events_array)){
    $events_us_active="active";
}elseif (in_array($url_segment,$cms_pages_array)){
    $cms_pages_active="active";
}elseif (in_array($url_segment,$slider_array)){
    $slider_active="active";
}


?>
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <!-- Meta data -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Language" content="en"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"/>

    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="<?php echo dt_get_fevicon(); ?>"/>
    <!-- Title -->
    <title><?php echo isset($title) ? $title : ""; ?></title>

    <!-- Style css -->
    <link href="<?php echo base_url('assets/global/css/bootstrap.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/style.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/default.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/horizontal-menu/horizontal.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/icons.css');?>">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">

    <script>
        var base_url="<?php echo base_url(); ?>";
    </script>
</head>
<body>
<!-- Loader -->
<div id="loading">
    <img src="<?php echo base_url('assets/admin/images/other/loader.svg');?>" class="loader-img" alt="Loader">
</div>

<div class="page">
    <div class="page-main">

        <!-- Top-header opened -->
        <div class="header-main header sticky">
            <div class="app-header header top-header navbar-collapse ">
                <div class="container-fluid">
                    <a id="horizontal-navtoggle" class="animated-arrow hor-toggle"><span></span></a><!-- sidebar-toggle-->
                    <div class="d-flex">
                        <a class="header-brand" href="<?php echo base_url('admin/dashboard') ?>">
                            <img src="<?php echo dt_get_CompanyLogo(); ?>" class="header-brand-img desktop-logo" alt="<?php echo dt_get_CompanyName(); ?>">
                            <img src="<?php echo dt_get_CompanyLogo(); ?>" class="header-brand-img desktop-logo-1" alt="<?php echo dt_get_CompanyName(); ?>">
                            <img src="<?php echo dt_get_CompanyLogo(); ?>" class="mobile-logo" alt="<?php echo dt_get_CompanyName(); ?>">
                            <img src="<?php echo dt_get_CompanyLogo(); ?>" class="mobile-logo-1" alt="<?php echo dt_get_CompanyName(); ?>">
                        </a>
                        <div class="d-flex header-right ml-auto">
                            <div>
                                <a class="nav-link icon" href="<?php echo base_url(); ?>" target="_blank">
                                    <i class="fe fe-external-link"></i>
                                </a>
                            </div><!-- Fullscreen -->

                            <div class="dropdown drop-profile">
                                <a class="nav-link pr-0 leading-none" href="#" data-toggle="dropdown" aria-expanded="false">
                                    <div class="profile-details mt-1">
                                        <span class="mr-3 mb-0  fs-15 font-weight-semibold"><?php echo $login_data['first_name'] . " " . $login_data['last_name']; ?></span>
                                    </div>
                                    <img class="avatar avatar-md brround" src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="image">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow animated bounceInDown w-250">
                                    <div class="user-profile bg-header-image border-bottom p-3">
                                        <div class="user-image text-center">
                                            <img class="user-images" src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="image">
                                        </div>
                                        <div class="user-details text-center">
                                            <h4 class="mb-0"><?php echo $login_data['first_name'] . " " . $login_data['last_name']; ?></h4>
                                            <p class="mb-1 fs-13 text-white-50"><?php echo $login_data['email'];?></p>
                                        </div>
                                    </div>
                                    <a class="dropdown-item" href="<?php echo base_url('admin/profile'); ?>">
                                        <i class="dropdown-icon mdi mdi-account-outline "></i> <?php echo dt_translate('profile');?>
                                    </a>
                                    <a class="dropdown-item" href="<?php echo base_url('admin/change-password'); ?>">
                                        <i class="dropdown-icon  mdi mdi-settings"></i> <?php echo dt_translate('Change_password');?>
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item mb-1" href="<?php echo base_url('admin/logout'); ?>">
                                        <i class="dropdown-icon mdi  mdi-logout-variant"></i> <?php echo dt_translate('logout');?>
                                    </a>
                                </div>
                            </div><!-- Profile -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top-header closed -->

        <!-- Horizontal-menu -->
        <div class="horizontal-main hor-menu clearfix">
            <div class="horizontal-mainwrapper container-fluid clearfix">
                <nav class="horizontalMenu clearfix">
                    <ul class="horizontalMenu-list">

                        <li aria-haspopup="true"><a href="<?php echo base_url('admin/dashboard'); ?>"><i class="fe fe-home"></i> Dashboard</a></li>

                        <?php if (in_array($current_page_slug,$front_end_slug) && $this->login_type=='A'): ?>

                            <li class="nav-item">
                                <a class="nav-link <?php echo $home_content_active; ?> show" href="<?php echo base_url('admin/manage-home-content'); ?>" >
                                    <i class="fe fe-globe"></i> <?php echo dt_translate('home')." ".dt_translate('content'); ?>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link <?php echo $banner_image_active; ?> show" href="<?php echo base_url('admin/manage-banner'); ?>" >
                                    <i class="fe fe-image"></i> <?php echo dt_translate('banner_image'); ?>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a  class="nav-link <?php echo $about_us_active; ?> show" href="<?php echo base_url('admin/manage-about-us'); ?>" >
                                    <i class="fe fe-globe"></i> <?php echo dt_translate('about_us'); ?>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo $cms_pages_active; ?> show" href="<?php echo base_url('admin/manage-pages'); ?>" >
                                    <i class="fe fe-codepen"></i> <?php echo dt_translate('pages'); ?>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a  class="nav-link <?php echo $gallery_active; ?> show" href="<?php echo base_url('admin/manage-gallery'); ?>" aria-selected="false">
                                    <i class="fe fe-image"></i> <?php echo dt_translate('gallery'); ?>
                                </a>
                            </li>
<!--                            <li class="nav-item">-->
<!--                                <a  class="nav-link --><?php //echo $slider_active; ?><!-- show" href="--><?php //echo base_url('admin/manage-slider'); ?><!--" aria-selected="false">-->
<!--                                    <i class="fe fe-image"></i> --><?php //echo dt_translate('slider'); ?>
<!--                                </a>-->
<!--                            </li>-->

                            <?php if(is_module_enabled('team')==true): ?>
                                <li class="nav-item">
                                    <a  class="nav-link <?php echo $team_active; ?> show" href="<?php echo base_url('admin/manage-team'); ?>" aria-selected="false">
                                        <i class="fe fe-users"></i> <?php echo dt_translate('team'); ?>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if(is_module_enabled('news')==true): ?>
                                <li class="nav-item">
                                    <a  class="nav-link <?php echo $news_active; ?> show" href="<?php echo base_url('admin/manage-news'); ?>" aria-selected="false">
                                        <i class="fe fe-package"></i> <?php echo dt_translate('news'); ?>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if(is_module_enabled('events')==true): ?>
                                <li class="nav-item">
                                    <a  class="nav-link <?php echo $events_us_active; ?> show" href="<?php echo base_url('admin/manage-events'); ?>" aria-selected="false">
                                        <i class="fe fe-code"></i> <?php echo dt_translate('events'); ?>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if(is_module_enabled('projects')==true): ?>
                                <li class="nav-item">
                                    <a  class="nav-link <?php echo $projects_us_active; ?> show" href="<?php echo base_url('admin/manage-projects'); ?>" aria-selected="false">
                                        <i class="fe fe-server"></i> <?php echo dt_translate('projects'); ?>
                                    </a>
                                </li>
                            <?php endif; ?>



                            <li class="nav-item">
                                <span class="mt-3 mt-md-0 pg-header">
                                    <a href="<?php echo base_url('admin/dashboard'); ?>" class="btn btn-danger custom_add_donation_button "><i class="fe fe-x"></i></i></a>
                                </span>
                            </li>



                        <?php else: ?>
                            <?php if($this->login_type=='A'): ?>
                                <li aria-haspopup="true"><a href="<?php echo base_url('admin/manage-home-content'); ?>"><i class="fe fe-globe"></i> <?php echo dt_translate('frontend'); ?></a></li>
                            <?php endif; ?>

                            <li>
                                <a href="<?php echo base_url('admin/donation'); ?>"><i class="fe fe-heart"></i> <?php echo dt_translate('donation'); ?></a>
                            </li>

                            <?php if(is_module_enabled('item')==true): ?>
                                <li>
                                    <a href="<?php echo base_url('admin/item-donation'); ?>"><i class="fe fe-heart"></i> <?php echo dt_translate('item')." ".dt_translate('donation'); ?></a>
                                </li>
                            <?php endif; ?>

                            <?php if(is_module_enabled('causes')==true && $this->login_type=='A'): ?>
                                <li>
                                    <a href="<?php echo base_url('admin/manage-causes'); ?>"><i class="fe fe-activity"></i> <?php echo dt_translate('causes'); ?></a>
                                </li>
                            <?php endif; ?>

                            <?php if(is_module_enabled('expense')==true): ?>
                                <li>
                                    <a href="<?php echo base_url('admin/expense'); ?>"><i class="fe fe-credit-card"></i> <?php echo dt_translate('expense'); ?></a>
                                </li>
                            <?php endif; ?>

                            <?php if(is_module_enabled('report')==true && $this->login_type=='A'): ?>
                                <li>
                                    <a href="<?php echo base_url('admin/report'); ?>"><i class="fe fe-bar-chart"></i> <?php echo dt_translate('report'); ?></a>
                                </li>
                            <?php endif; ?>

                            <?php if(is_module_enabled('staff')==true && $this->login_type=='A'): ?>
                                <li><a href="<?php echo base_url('admin/staff'); ?>"  class="sub-icon"><i class="fe fe-user"></i> <?php echo dt_translate('staff');?></a></li>
                            <?php endif; ?>

                            <?php if($this->login_type=='A'): ?>
                                <li aria-haspopup="true"><a href="<?php echo base_url('admin/settings'); ?>"><i class="fe fe-settings"></i> <?php echo dt_translate('setting'); ?></a></li>
                            <?php endif; ?>

                            <li class="nav-item">
                                <a  class="nav-link" href="<?php echo base_url('admin/manage-contact-us'); ?>" aria-selected="false">
                                    <i class="fe fe-mail"></i> <?php echo dt_translate('contact_us'); ?>
                                </a>
                            </li>

                            <li aria-haspopup="true">
                                <span class="mt-3 mt-md-0 pg-header">
                                    <a href="<?php echo base_url('admin/add-donation'); ?>" class="btn btn-info custom_add_donation_button "><i class="fe fe-plus mr-1"></i><?php echo dt_translate('add') . " " . dt_translate('donation'); ?> </a>
                                </span>
                            </li>
                        <?php endif; ?>


                    </ul>
                </nav>
                <!--Nav end -->
            </div>
        </div>
        <!-- Horizontal-menu end -->
    </div>