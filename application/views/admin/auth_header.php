<!doctype html>
<html lang="en" dir="ltr">
<head>
    <!-- Meta data -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Language" content="en"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"/>

    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="<?php echo dt_get_fevicon(); ?>"/>
    <!-- Title -->
    <title><?php echo isset($title) ? $title : ""; ?></title>
    <!-- Style css -->
    <link href="<?php echo base_url('assets/global/css/bootstrap.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/style.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/default.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/horizontal-menu/horizontal.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/icons.css');?>">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script>
        var base_url="<?php echo base_url(); ?>";
    </script>
</head>
<body class="app h-100vh">
<!-- Loader -->
<div id="loading">
    <img src="<?php echo base_url('assets/admin/images/other/loader.svg');?>" class="loader-img" alt="Loader">
</div>