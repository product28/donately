<?php
include VIEWPATH . 'admin/auth_header.php';
?>
    <!-- Page opened -->
    <div class="page mt-5">
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="">
            <div class="col col-login mx-auto">
                <div class="text-center">
                    <a href="<?php echo base_url('admin/login');?>"><img src="<?php echo dt_get_CompanyLogo(); ?>" class="header-brand-img desktop-logo h-100 mb-5" alt="<?php echo dt_get_CompanyName(); ?>"></a>
                    <a href="<?php echo base_url('admin/login');?>"><img src="<?php echo dt_get_CompanyLogo(); ?>" class="header-brand-img dark-theme h-100 mb-5 " alt="<?php echo dt_get_CompanyName(); ?>"></a>
                </div>
            </div>
            <!-- container opened -->
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 justify-content-center mx-auto text-center">
                        <div class="card">

                            <div class="row">

                                <div class="col-md-12 col-lg-12 pl-0 ">
                                    <?php
                                    $attributes = array('id' => 'Login', 'name' => 'Login', 'method' => "post");
                                    echo form_open('admin/login-action', $attributes);
                                    ?>
                                    <div class="card-body p-6 about-con pabout">
                                        <div class="card-title text-center  mb-4"><?php echo dt_translate('login') ?></div>
                                        <?php $this->load->view('message'); ?>
                                        <div class="form-group">
                                            <input required  data-msg-email="<?php echo dt_translate('enter_valid_email'); ?>" data-msg-required="<?php echo dt_translate('required_message'); ?>" autocomplete="off" name="username" id="username"  placeholder="<?php echo dt_translate('email') ?>" type="email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input required  data-msg-required="<?php echo dt_translate('required_message'); ?>" autocomplete="off" name="password" id="password" placeholder="<?php echo dt_translate('password') ?>" type="password" class="form-control">
                                        </div>

                                        <div class="form-footer mt-1">
                                            <button class="btn btn-success btn-block" type="submit">
                                                <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                                <?php echo dt_translate('login'); ?>
                                            </button>
                                        </div>
                                        <hr class="divider">
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <a href="<?php echo base_url("admin/forgot-password"); ?>" class="float-right small text-info"><?php echo dt_translate('forgot_password'); ?>?</a>
                                            </label>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- container closed -->
        </div>
    </div>
    <!-- Page closed -->
<?php
include VIEWPATH . 'admin/auth_footer.php';
?>