<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Staff extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('model_admin');
        $this->authenticate->check_admin();

        $item_donation=is_module_enabled('staff');
        if($item_donation==false){
            redirect('admin/dashboard');
        }
    }

    //show admin list
    public function index() {
        $data['title'] = dt_translate('manage') . " " . dt_translate('staff');
        $order = "created_on  DESC";
        $admin = $this->model_admin->getData("app_admin", "*", "type='S'", "", $order);
        $data['admin_data'] = $admin;
        $this->load->view('admin/staff/index', $data);
    }

    //show add admin form
    public function add_staff() {
        $data['title'] = dt_translate('add') . " " . dt_translate('admin');
        $this->load->view('admin/staff/add_update', $data);
    }

    //show edit admin form
    public function update_staff($id) {
        $id = (int) $id;
        $admin = $this->model_admin->getData("app_admin", "*", "id='$id'");
        if (empty($admin)) {
            $this->session->set_flashdata('msg', dt_translate('invalid_request'));
            $this->session->set_flashdata('msg_class', 'failure');
            redirect('admin/staff');
        }

        if (isset($admin[0]) && !empty($admin[0])) {
            $data['customer_data'] = $admin[0];
            $data['title'] = dt_translate('update') . " " . dt_translate('admin');
            $this->load->view('admin/staff/add_update', $data);
        } else {
            redirect('admin/staff');
        }
    }

    //add/edit an admin
    public function save_staff() {

        $admin_id = $this->session->userdata("ADMIN_ID");

        $id = (int) $this->input->post('id', true);
        $this->form_validation->set_rules('first_name', dt_translate('first_name'), 'trim|required');
        $this->form_validation->set_rules('last_name', dt_translate('last_name'), 'trim|required');
        $this->form_validation->set_rules('email', dt_translate('email'), 'trim|required|is_unique[app_admin.email.id.' . $id . ']');
        $this->form_validation->set_rules('phone', dt_translate('phone'), 'trim|required|min_length[10]|max_length[10]|is_unique[app_admin.phone.id.' . $id . ']');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == false) {
            if ($id > 0) {
                $this->update_staff();
            } else {
                $this->add_staff();
            }
        } else {

            $data['first_name'] = $this->input->post('first_name', true);
            $data['last_name'] = $this->input->post('last_name', true);
            $data['email'] = $this->input->post('email', true);
            $data['phone'] = $this->input->post('phone', true);
            $data['status'] = $this->input->post('status', true);
            if ($id > 0) {
                $data['updated_on'] = date("Y-m-d H:i:s");
                $this->model_admin->update('app_admin', $data, "id=" . $id);

                $this->session->set_flashdata('msg', dt_translate('record_update'));
                $this->session->set_flashdata('msg_class', 'success');
                redirect('admin/staff', 'redirect');
            } else {
                $this->load->helper('string');
                $password = random_string('alnum', 8);

                $data['password'] = md5($password);
                $data['type'] = "S";
                $data['created_by'] = $admin_id;
                $data['created_on'] = date("Y-m-d H:i:s");

                $name = (trim($this->input->post('first_name'))) . " " . (trim($this->input->post('last_name')));
                $hidenuseremail = $this->input->post('email');
                $this->model_admin->insert('app_admin', $data);

                $subject = dt_translate('staff') . " | " . dt_translate('account_registration');
                $define_param['to_name'] = $name;
                $define_param['to_email'] = $hidenuseremail;

                $parameter['NAME'] = $name;
                $parameter['LOGIN_URL'] = base_url('login');
                $parameter['EMAIL'] = $hidenuseremail;
                $parameter['PASSWORD'] = $password;

                $html = $this->load->view("email_template/registration_admin", $parameter, true);
                $send = $this->sendmail->send($define_param, $subject, $html);

                $this->session->set_flashdata('msg', dt_translate('record_insert'));
                $this->session->set_flashdata('msg_class', 'success');
                redirect('admin/staff', 'redirect');
            }
        }
    }

    //delete an admin
    public function delete_staff($id) {
        $id = (int) $id;
        if ($id > 0) {
            $this->model_admin->delete('app_admin', 'id=' . $id);
            $this->session->set_flashdata('msg', dt_translate('record_delete'));
            $this->session->set_flashdata('msg_class', 'success');
            echo true;
        } else {
            echo FALSE;
        }
    }

}
